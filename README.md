RZE (rise) Engine
=====
A personal project I'm using to test things out and program cool stuff in my spare time. 

Much of the code is subject to refactoring and all tasks are logged in a Clubhouse.io project so I don't forget.

If you have interest in the code, or have any suggestions for me, please e-mail covedrop@gmail.com.